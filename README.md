# Websocket PHP Ratchet

## Dependency
- server : cboden/ratchet
- client : ratchet/pawl

## Running server  :
- php server/bin/server.php

## Test Rule : 
- open http://pathdomain/client
- curl (insomnia/postman) to http://pathdomain/client/service.php?id=1&msg=pesan&nama=addin (you can custom get with what do you want)
- or directly open browser http://pathdomain/client/service.php?id=1&msg=pesan&nama=addin
- must be show desktop notification

## Describe : 
- just a showing desktop notification
- you can custom everything
